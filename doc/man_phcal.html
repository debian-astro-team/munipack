<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- meta -->
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A general astronomical image processing software">
<meta name="author" content="Filip Hroch">
<link href="news_feed.xml" type="application/atom+xml" rel="alternate" title="Sitewide ATOM Feed" />
<link rel="stylesheet" href="munipack.css">
<link rel="shortcut icon" href="favicon.ico">
<title>Munipack ‒ Photometry Calibration</title>
</head>
<body>
<header>
  <a href="munipack.html"><img src="big_logo.png" alt="Munipack's logo" class="head"></a>
  <div class="headtitles">
    <p class="head">
      <a class="headtitle" href="munipack.html">Munipack</a>
    </p>
    <p class="head">
      <a class="headsubtitle" href="munipack.html">The astronomical image processing software</a>
    </p>
  </div>
  <div class="buttons">
    <a href="guide.html" class="button">
      <div class="bicon">
	📘
      </div>
      <div class="hide">
	Guide
      </div>
    </a>
    <a href="docs.html" class="button">
      <div class="bicon">
	📁
      </div>
      <div class="hide">
	Documents
      </div>
    </a>
  </div>
</header>
<section>

<h1>Photometry Calibration</h1>

<p class="abstract">
A photometry calibration.
</p>

<!--
<p>
The calibration can be applied on both single- and
multi-filter observations.
</p>
-->

<h2>Synopsis</h2>

<code>
munipack phcal [.. parameters ..] file(s)[,result(s)]
</code>

<h2>Description</h2>

<p>
Purpose of this routine is the photometry calibration
of CCD frames. The calibration establish
an exact relation between observed
instrumental counts and expected amount of photons
by using a (small) set of known calibration stars.
On base of the calibration, all known objects
and the image itself should be transformed from
instrumental counts to aboslute fluxes of photons,
energy or magnitudes.
</p>

<h2>Detailed Description</h2>

<p>Every single picture elements exposed by light for a time period
is collecting electrons created due to photo-electric effect
from photons. Counts of the electrons is digitised and stored
in output images. One can be directly used for additional light analysis.
We will use term (instrumental) count(s) for their calculated amount
(widely used by astronomers).
</p>

<p>
For a non-ideal detector, amount of digitised electrons will less
than for photons. These counts will depends on the instrumental equipment
(quality of optical path, lenses, mirrors, weather, quantum efficiency
of CCD camera, etc). As consequence of the measurement process,
the counts is unique per an apparatus and observation conditions.
Two telescopes equipped with the same instrumentation will not product
the same counts due to atmospheric conditions. The different apparatus
(filters, detectors) generally produces different counts. To compare
and to process observations from different observatories, we need
unify all observations to same scales. Traditionally, in various
parts of physics, the calibration is done with setting of calibration
scale on a priory known calibrated sources. The sources are calibration
stars for astronomers. The scale is relation between observed counts and
produced photons.
</p>

<p>
More detailed description of the calibration can be found in
<a href="phoverview.html">Photometry Overview</a>.
</p>

<p>
The calibration implemented by Munipack follows these steps:
</p>
<ul>
<li>Detected stars are merged using of coordinates
   against to a reference photometric catalogue.</li>
<li>With known filter and photometry system,
    expected count of photons is determined from magnitudes
    in the catalogue.</li>
<li>Instrumental counts are transformed
    from an instrumental to a standard (photometry system)
    filter using of provided transformation
    matrix. (This step is optional.)</li>
<li>By using of a telescope area, gain and exposure time are
    observed counts normalised on count rates.</li>
<li>The calibration itself is determined as the ratio of
    reference amount of photons and observed rates.</li>
</ul>

<p>
Image data values are converted on a physical quantity
(like photons, fluxes, magnitudes). The conversion is applied
on frame data as well as on photometric tables. The conversion
requires known astrometry and photometry calibration.
</p>

<p>
The conversion is very useful for converting observed quantities
to a derived ones. One is ideal for construction of multi-spectral
pictures of objects.
</p>

<h2>Algorithm</h2>

<p>
The calibration is computed by the way:
</p>
<ul>
<li>Reference catalogue and frames are merged in spherical coordinates
and by search in close neighbourhood.</li>
<li>From known filter and photometry system, from catalogue magnitudes
photon fluxes are computed. </li>
<li>Observed counts are normalised to rates by knowing of telescope
area and exposure time.</li>
<li>The calibration ratio between expected photons and observed counts
    is determined by a robust method.</li>
<li>All values in frames and tables are recomputed on photons.</li>
</ul>

<p>When photometry transformation table from instrumental to
standard system is available <samp>--tratab</samp>, the counts
are transformed from instrumental to a relative standard counts
and the calibration is performed on the kind of counts.
</p>


<h2 id="modes">Operational modes</h2>

<p>
Photometry calibration is a very complex task so various
ways are implemented:
</p>

<dl>
<dt>Reference Catalogue</dt><dd>Reference sources are stars provided
as a table. The table can be prepared by hand or a photometrical
catalogue can be used. Precision of calibration is given by quality
of the catalogue and also by true colour transformation of instrumental
to the catalogue. Usually, the calibration has small systematical
offsets but larger random errors.</dd>
<dt>Reference Frame</dt><dd>Reference stars are pulled from previsouly
calibrated frame. Relative precision is generally better than in previous
case because no colour transformation is required.</dd>
<dt>Manual Calibration</dt><dd>The calibration values are provided
by an user.</dd>
</dl>

<p>The choice suitable for specific situation would be very difficoult.
</p>

<h2>Prerequisites</h2>
<p>
Needs both astrometry and instrumental photometry of frames.
</p>

<p>
Headers would contain all the exposure time, filter, telescope area and photometry
system keywords. 
</p>

<p>
Specify photometric system (a conventional
set of filters). Default is used value from frame header, use it when value is missing 
or needs correction. The option is important while determining of photometry calibration.
</p>

<p>
Specify filter. Default is used value from frame header, use it when value is missing or needs correction. The filter
 is important while determining of photometry calibration.
</p>

<p>
When calibrated frame contains FWHM parameter, the first aperture larger
then the radius is used. When the parameter missing, the first aperture or user
provided aperture is used.
</p>


<p><b>Important.</b>
<p>
The exposure time, filter, gain, area and an instrumental photometry system 
are absolutely necessory for calibration and none of them can not be omitted. 
At first, all values are obtained by reading of headers of FITS files. 
If at least one is not found, the calibration 
process is stopped (a wrong calibration which looks as valid is much more worse 
than any fail).
</p>
<p>
The situation can be solved by the ways:
</p>
<ul>
<li>The values can be provided by editing of input files with help
    of <samp>fits</samp> module:
<pre>
$ munipack fits --key-update AREA=1,'[m] telescope area' huge.fits
</pre>
for all missing parameters.
 </li>
<li>
Also convenience options for most frequent missing parameters are
provided: area and instrumental photosystem:
<pre>
$ munipack phcal ... --area 1 --photsys-instr 'MONTEBOO' ... frames.fits
</pre>
The convenience options doesn't supply common keywords (exptime, filter
and gain) which can be usually found in frames.
</li>
</ul>

<p>
While common values of exposure times, filters etc. are included to every
header, the keywords can differ from Munipack's defaults. In the case,
set ones via <a href="man_env.html">environment variables</a>.
</p>

<h2>Results</h2>

<p>
Output calibrated fits frames contains a new extension
described in <a href="dataform_photometry.html">Photometry Calibrated File</a>.
Its table contains coordinates of stars on frames and various
photometric quantities.
</p>


<!--
<h2>Input And Output</h2>

<p>
On input, list of frames with valid astrometry and containing the table with
photometry is expected.
</p>

<p>
On output, a new list of FITS files is created from input files.
</p>
-->

<h2>Parameters</h2>

<h3>Reference Catalogue:</h3>
<dl>
<dt><samp>-C,--cal</samp></dt><dd>The calibration is specified by
an exterior entity. The values are saved to a PHOTOMETRY extension
header and counts are converted to photons for both image and table
values.
</dd>
<dt><samp>-c,--cat</samp></dt><dd>Reference photometric catalogue. A fits table with
coordinates and magnitudes of reference stars. One can be a selection from a "real"
catalogue or a table prepared by oneself.
See section <a href="man_phcal.html#prep">Preparation of Photometric Catalogue</a>,
how create this table by hand.
</dd>
<dt><samp>-r,--ref</samp></dt><dd>Reference frame is used. The reference frame
usually created by <samp>-c,-C</samp> options. Useful for relative photometry.</dd>
<dt><samp>-f,--filters</samp></dt><dd>Filters used for calibration. </dd>
<dt><samp>-F,--filter-ref</samp></dt><dd>Reference filter for calibration.</dd>
<dt><samp>--col-ra</samp></dt><dd>Right Ascension column. Default is RAJ2000.</dd>
<dt><samp>--col-dec</samp></dt><dd>Declination column. Default is DEJ2000.</dd>
<dt><samp>--col-mag</samp></dt><dd>Magnitude column(s). Default is the filter
    (-f option) with 'mag' suffix like <samp>-f V --col-mag Vmag</samp> (?).</dd>
<dt><samp>--col-magerr</samp></dt><dd>Magnitude std. error column(s) (no default).
    If this parameter is omited, errors are estimated
    as the square of photons derived from magnitudes.</dd>
<dt><samp>--tol</samp></dt><dd>search radius for object identification in degrees
                               (default 5*FWHM) </dd>
</dl>

<h3>Calibration specific:</h3>
<dl>
<dt><samp>--photsys-ref</samp></dt><dd>reference (standard) photometric system
          (catalogue)</dd>
<dt><samp>--photsys-instr</samp></dt><dd>instrumental photometric system (frames)</dd>
<dt><samp>-q, --quantity</samp></dt><dd>
     calibrated quantities by default: PHRATE, FLUX, FNU, FLAM, MAG, ABMAG, STMAG
                 (<a href="dataform_photometry.html#phquantities">see description</a>),
            multiple quantities can be used, separated by colon</dd>
</dl>

<h3>Photometric system:</h3>
<dl>
<dt><samp>--tratab</samp></dt><dd>Table describing conversion from instrumental
  to reference photo-system. Usually product of <a href="man_phfotran.html">
phfotran</a>.</dd>
<dt><samp>--phsystab</samp></dt><dd>A table with photometric system definitions
(<a href="dataform_photosys.html">specification</a>)</dd>
<dt><samp>--list</samp></dt><dd>Lists available photometric systems. Their identifiers
                                are names of extensions in
(<a href="dataform_photosys.html">photometric system definitions</a>) file.</dd>
</dl>

<h3>Common:</h3>

<dl>
  <dt><samp>-th, --threshold</samp></dt><dd>Select stars on both reference
    and calibrated frames (not applied on catalogues) with its stellar
    flux to a sky noise (signal to noise) ratio greater then
    the threshold value. Both the values are determined in the same aperture.
    The proper
    choice is crucial for photometry precision because it helps to select
    only bright stars with minimal pollution by the sky noise. The default
    value 5 is very conservative setup. It is suitable for bad conditions
    (like urban or a full moon light observations). Standard and good
    conditions will allow lower ratios. In an ideal case, it can be
    under one for best results.
     </dd>
  <dt><samp>-e, --maxerr</samp></dt><dd>Select stars on both reference
    and calibrated frame (not applied on catalogues) with its relative
    flux error smaller than the error limit. The value does not limits
    the final precision, but limits fluency of noisy data.
    The default value 0.1 (ten percents, about tenth of magnitude)
    will also include relative imprecise stars into both calibration and
    calibrated set. The values under 0.01 and lower will select
    only precise and suitable calibration stars.
     </dd>
<dt><samp>--area</samp></dt><dd>Area of telescope aperture in square meters [m2].</dd>
<dt><samp>--saper</samp></dt><dd>selects appropriate aperture from PHOTOMETRY
    extension. By default, flux in infinite aperture is used.</dd>
  <dt><samp>--apcorr</samp></dt><dd>sets the aperture correction.
    This correction converts fluxes in an aperture to the total flux
    (infinite aperture). The value of correction should be obtained
    by <a href="man_gphot.html">gphot</a>.</dd>

<dt><samp>--advanced</samp></dt><dd>Advanced format. Additional extensions
                    (results of star find, photometry and residuals) are included.
                    This format is not used by default because result FITS
                    is twice or more bigger.
                  </dd>
</dl>


<p>See <a href="man_com.html">Common options</a> for input/output filenames.</p>

  <p>
    When options for the area and the reference and instrumental systems are used,
    FITS header is updated according to provided values.
  </p>

<p>
Default values for coordinates will be usually unsatisfactory.
</p>

<p>See <a href="man_com.html">Common options</a> for input/output filenames.
If advanced parameters <samp>-O</samp> are not set, default
<samp>-O --mask '!\1_XXX.\2'</samp> is used according to some selected quantity.
For example, <samp>-q MAG</samp> and the input file <samp>blazar_01R.fits</samp>
will produce the output file <samp>blazar_01R_MAG.fits</samp>. Be warned, that
output files are overwritten in any case by default.</p>


<!--
<h2>Filters Selection</h2>
<p>
The correspondence between filter in reference catalogue and
frames is selected just only on base of their order: first specified
label (via \-\-col-mag) correspond do first image, etc.. The designations
in catalogue and filter in FITS frame are not compared by any
way.
</p>

<p>
For example, bands in reference table
are labelled as Bmag, Vmag and filters as B,V (filters
are for clarity included also in FITS names, but unchecked):
</p>
<pre>
$ munipack phcal ... \-\-col-mag Bmag,Vmag frame_B.fits frame_V.fits
</pre>
-->


<h2 id="prep">Preparation Of Photometry Catalogue</h2>

<p>
The table is a fits table and must contains columns with Right Ascension,
Declination, reference magnitudes (and optionally with standard errors
of the magnitudes). The column naming is by default RA, DEC, MAG and can
be changed with --col-* options.
</p>

<p>
The table may be prepared by any standard FITS utility (for example
<a href="http://heasarc.gsfc.nasa.gov/lheasoft/ftools/fhelp/fcreate.txt">fcreate</a>
utility of
<a href="http://heasarc.gsfc.nasa.gov/docs/software/lheasoft/ftools/">FTOOLS</a>).
</p>

<p>
To save the time, you can just edit file <samp>mtable.lst</samp> in
Munipack distribution (carefully handle with NAXIS1 and NAXIS2, etc.)
and create a table
</p>
<pre>
$ munipack fits --restore mtable.lst
</pre>
<p>
The output in mtable.fits can be used to a right frame as
</p>
<pre>
$ munipack phcal -c mtable.fits frame.fits
</pre>

<h2>A Low-Precision Calibration</h2>
<p>
We are using just one filter. No transformation matrix is used.
</p>

<h2>A High-Precision Calibration</h2>
<p>
We are using multiple filters. Transformation matrix is used,
we get the maximum possible precision.
</p>


<h2>Examples</h2>

<p>
Calibrate against to UCAC5 catalogue:
</p>
<pre>
$ munipack cone -c UCAC5 -o 0716cat.fits -r 0.1 110.47 71.34
$ munipack phcal -c 0716cat.fits --col-ra RAJ2000 --col-dec DEJ2000 \
  --col-mag Gmag  --photosys Johnson 0716_?R.fits
$ munipack phcal -c T_Phe.fits --tratab phfotran.fits  --col-mag B,V,R,I \
   TPhe_B.fits,b.fits TPhe_V.fits,v.fits TPhe_R.fits,r.fits TPhe_I.fits,i.fits
</pre>

<pre>
munipack phcal --verbose -r grb140423-2_C_0002_cal.fits --photsys-ref Johnson  grb140423-2_C_0003.fits
</pre>


<h2>See Also</h2>
<p>
<a href="man_phfotran.html">Photometic System Transformation</a>,
<a href="man_com.html">Common Options</a>,
<a href="dataform_photometry.html">Photometry Format</a>,
<a href="phoverview.html">Photometry Overview</a>.
</p>

</section>
<footer>
  © 1997 – 2025
  <a href="https://integral.physics.muni.cz/" title="author's homepage"
   class="foot">Filip Hroch</a>
</footer>
</body>
</html>
