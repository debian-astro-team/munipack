<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- meta -->
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A general astronomical image processing software">
<meta name="author" content="Filip Hroch">
<link href="news_feed.xml" type="application/atom+xml" rel="alternate" title="Sitewide ATOM Feed" />
<link rel="stylesheet" href="munipack.css">
<link rel="shortcut icon" href="favicon.ico">
<title>Munipack ‒ Photometric System Transformation</title>
</head>
<body>
<header>
  <a href="munipack.html"><img src="big_logo.png" alt="Munipack's logo" class="head"></a>
  <div class="headtitles">
    <p class="head">
      <a class="headtitle" href="munipack.html">Munipack</a>
    </p>
    <p class="head">
      <a class="headsubtitle" href="munipack.html">The astronomical image processing software</a>
    </p>
  </div>
  <div class="buttons">
    <a href="guide.html" class="button">
      <div class="bicon">
	📘
      </div>
      <div class="hide">
	Guide
      </div>
    </a>
    <a href="docs.html" class="button">
      <div class="bicon">
	📁
      </div>
      <div class="hide">
	Documents
      </div>
    </a>
  </div>
</header>
<section>

<h1>Photometric System Transformation</h1>

<p class="abstract">
An approximation of photon fluxes in
a standard set of filters by a linear combination of
an instrumental set of filters
is determined on a field with known calibration sources.
</p>

<h2>Synopsis</h2>

<code>
munipack phfotran [.. parameters ..] file(s)
</code>

<h2>Description</h2>

<p>
A common astronomical apparatus composed from a telescope, filter
and a detector has slightly different spectral sensitivity than
the standard one which had established the primary (stellar)
standards of a photometric system. Fortunately, a commonly used
equipment close fits the standard spectral sensitivity due to
effort of manufactures. Therefore, any differences
are small and can be, with suitable precision, approximated by
a linear approximation.
</p>

<p>
This action determines such transformation by application
of the linear approximation between observed sum of counts and
expected photons from calibration stars.
</p>

<p>
The transformation table can be used to convert observed counts
<i>c</i> in an instrumental system (identified by PHOTSYS1) to
counts <i>c'</i> in a standard system (identified by PHOTSYS2).
</p>
<p>
<i>
c'<sub>i</sub> = Σ<sub>j</sub> C<sub>ij</sub> c<sub>j</sub>, i = { B,V ...}
</i>
</p>

<p>
The transformed counts <i>c'</i> will generally proportional to
observed photons and can be used for calibration.
</p>

<p>
The transformation is designed to be used on a calibration field.
The sparse field with many of well calibrated stars. There are sources
of such fields (which can be supposed as the secondary standards):
</p>
<ul>
<li><a href="http://www3.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/community/STETSON/standards/">Stetson's fields</a></li>
</ul>



<h2>Algorithm</h2>

<p>
The transformation is determined by the way:
</p>
<ul>
<li>Reference catalogue and frames are merged in spherical coordinates
with nearly positions.</li>
<li>From known filter in a photometric system and catalogue magnitude,
photon fluxes are derived.</li>
<li>Observed counts are normalized to rates using of both telescope
area and exposure time.</li>
<li>The transformation matrix is determined.</li>
</ul>

<p>
A result of the transformation is a nearly tri-diagonal matrix (elements around
diagonal dominates over other ones). The limitation of the shape is forced
due to ill-conditioning of the problem.
</p>


<h2>Prerequisites</h2>
<p>
Needs both astrometry and instrumental photometry of frames.
</p>

<p>
Headers would contain all the exposure time, filter, telescope area and photometry
system keywords. 
</p>

<p>
Specify photometric system (a conventional
set of filters). Default is used value from frame header, use it when value is missing 
or needs correction. The option is important while determining of photometry calibration.
</p>

<p>
Specify filter. Default is used value from frame header, use it when value is missing or needs correction. The filter
 is important while determining of photometry calibration.
</p>

<p>
When calibrated frame contains FWHM parameter, the first aperture larger
then the radius is used. When the parameter missing, the first aperture or user
provided aperture is used.
</p>


<p><b>Important.</b>
<p>
The exposure time, filter, gain, area and an instrumental photometry system 
are absolutely necessory for calibration and none of them can not be omitted. 
At first, all values are obtained by reading of headers of FITS files. 
If at least one is not found, the calibration 
process is stopped (a wrong calibration which looks as valid is much more worse 
than any fail).
</p>
<p>
The situation can be solved by the ways:
</p>
<ul>
<li>The values can be provided by editing of input files with help
    of <samp>fits</samp> module:
<pre>
$ munipack fits --key-update AREA=1,'[m] telescope area' huge.fits
</pre>
for all missing parameters.
 </li>
<li>
Also convenience options for most frequent missing parameters are
provided: area and instrumental photosystem:
<pre>
$ munipack phcal ... --area 1 --photsys-instr 'MONTEBOO' ... frames.fits
</pre>
The convenience options doesn't supply common keywords (exptime, filter
and gain) which can be usually found in frames.
</li>
</ul>

<p>
While common values of exposure times, filters etc. are included to every
header, the keywords can differ from Munipack's defaults. In the case,
set ones via <a href="man_env.html">environment variables</a>.
</p>

<h2>Input And Output</h2>

<p>
On input, FITS frames in several filters are
required. Ones must be passed in order from short- to long-wavelengths.
Composited frames are recommended.
</p>

<p>
On output, a new FITS table representing the transformation is created.
</p>

<h2>Parameters</h2>

<h3>Reference Catalogue:</h3>
<dl>
<dt><samp>-C,--cal</samp></dt><dd>The calibration is specified by
an exterior entity. The values are saved to a PHOTOMETRY extension
header and counts are converted to photons for both image and table
values.
</dd>
<dt><samp>-c,--cat</samp></dt><dd>Reference photometric catalogue. A fits table with
coordinates and magnitudes of reference stars. One can be a selection from a "real"
catalogue or a table prepared by oneself.
See section <a href="man_phcal.html#prep">Preparation of Photometric Catalogue</a>,
how create this table by hand.
</dd>
<dt><samp>-r,--ref</samp></dt><dd>Reference frame is used. The reference frame
usually created by <samp>-c,-C</samp> options. Useful for relative photometry.</dd>
<dt><samp>-f,--filters</samp></dt><dd>Filters used for calibration. </dd>
<dt><samp>-F,--filter-ref</samp></dt><dd>Reference filter for calibration.</dd>
<dt><samp>--col-ra</samp></dt><dd>Right Ascension column. Default is RAJ2000.</dd>
<dt><samp>--col-dec</samp></dt><dd>Declination column. Default is DEJ2000.</dd>
<dt><samp>--col-mag</samp></dt><dd>Magnitude column(s). Default is the filter
    (-f option) with 'mag' suffix like <samp>-f V --col-mag Vmag</samp> (?).</dd>
<dt><samp>--col-magerr</samp></dt><dd>Magnitude std. error column(s) (no default).
    If this parameter is omited, errors are estimated
    as the square of photons derived from magnitudes.</dd>
<dt><samp>--tol</samp></dt><dd>search radius for object identification in degrees
                               (default 5*FWHM) </dd>
</dl>

<h3>Calibration specific:</h3>
<dl>
<dt><samp>--photsys-ref</samp></dt><dd>reference (standard) photometric system
          (catalogue)</dd>
<dt><samp>--photsys-instr</samp></dt><dd>instrumental photometric system (frames)</dd>
<dt><samp>-q, --quantity</samp></dt><dd>
     calibrated quantities by default: PHRATE, FLUX, FNU, FLAM, MAG, ABMAG, STMAG
                 (<a href="dataform_photometry.html#phquantities">see description</a>),
            multiple quantities can be used, separated by colon</dd>
</dl>

<h3>Photometric system:</h3>
<dl>
<dt><samp>--tratab</samp></dt><dd>Table describing conversion from instrumental
  to reference photo-system. Usually product of <a href="man_phfotran.html">
phfotran</a>.</dd>
<dt><samp>--phsystab</samp></dt><dd>A table with photometric system definitions
(<a href="dataform_photosys.html">specification</a>)</dd>
<dt><samp>--list</samp></dt><dd>Lists available photometric systems. Their identifiers
                                are names of extensions in
(<a href="dataform_photosys.html">photometric system definitions</a>) file.</dd>
</dl>

<h3>Common:</h3>

<dl>
  <dt><samp>-th, --threshold</samp></dt><dd>Select stars on both reference
    and calibrated frames (not applied on catalogues) with its stellar
    flux to a sky noise (signal to noise) ratio greater then
    the threshold value. Both the values are determined in the same aperture.
    The proper
    choice is crucial for photometry precision because it helps to select
    only bright stars with minimal pollution by the sky noise. The default
    value 5 is very conservative setup. It is suitable for bad conditions
    (like urban or a full moon light observations). Standard and good
    conditions will allow lower ratios. In an ideal case, it can be
    under one for best results.
     </dd>
  <dt><samp>-e, --maxerr</samp></dt><dd>Select stars on both reference
    and calibrated frame (not applied on catalogues) with its relative
    flux error smaller than the error limit. The value does not limits
    the final precision, but limits fluency of noisy data.
    The default value 0.1 (ten percents, about tenth of magnitude)
    will also include relative imprecise stars into both calibration and
    calibrated set. The values under 0.01 and lower will select
    only precise and suitable calibration stars.
     </dd>
<dt><samp>--area</samp></dt><dd>Area of telescope aperture in square meters [m2].</dd>
<dt><samp>--saper</samp></dt><dd>selects appropriate aperture from PHOTOMETRY
    extension. By default, flux in infinite aperture is used.</dd>
  <dt><samp>--apcorr</samp></dt><dd>sets the aperture correction.
    This correction converts fluxes in an aperture to the total flux
    (infinite aperture). The value of correction should be obtained
    by <a href="man_gphot.html">gphot</a>.</dd>

<dt><samp>--advanced</samp></dt><dd>Advanced format. Additional extensions
                    (results of star find, photometry and residuals) are included.
                    This format is not used by default because result FITS
                    is twice or more bigger.
                  </dd>
</dl>


<p>See <a href="man_com.html">Common options</a> for input/output filenames.</p>

  <p>
    When options for the area and the reference and instrumental systems are used,
    FITS header is updated according to provided values.
  </p>

<p>
Default values for coordinates will be usually unsatisfactory.
</p>

Add -E,--extin option description.

<h2>Caveats</h2>

<p>
Just equal number of instrumental and standard filters is implemented.
</p>



<h2>Examples</h2>

<p>
Calibrate against to UCAC5 catalogue:
</p>
<pre>
$ munipack cone -c UCAC5 -o 0716cat.fits  -r 0.1 110.47 71.34
$ munipack phfotran --area 1.86 --photsys-instr DK154 -c T_Phe.fits --col-ra RA --col-dec DEC --col-mag B,V,R,I T_Phe_000001.fits T_Phe_000003.fits T_Phe_000005.fits T_Phe_000007.fits
</pre>


<h2>See Also</h2>
<p>
<a href="man_com.html">Common options</a>,
<a href="man_phcal.html">Photometry Calibration</a>.
</p>

</section>
<footer>
  © 1997 – 2025
  <a href="https://integral.physics.muni.cz/" title="author's homepage"
   class="foot">Filip Hroch</a>
</footer>
</body>
</html>
