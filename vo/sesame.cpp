/*

  Virtual observatory Sesame CLI client

  Copyright © 2019 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "vosesame.h"
#include "votable.h"
#include <wx/wx.h>
#include <wx/txtstrm.h>
#include <wx/wfstream.h>
#include <wx/protocol/http.h>
#include <cstdio>


class Sesame: public wxAppConsole
{

public:
  bool OnInit();
  int OnRun();
};
IMPLEMENT_APP_CONSOLE(Sesame)

bool Sesame::OnInit()
{
  wxLog::DisableTimestamp();
  return true;
}

int Sesame::OnRun()
{
  bool verbose = false;
  wxString object, line;

  // limit logging
  wxLog::SetLogLevel(wxLOG_Message);
  wxLog::SetVerbose(); // important to activate previous setup

  wxFFileInputStream istream(stdin);
  wxTextInputStream input(istream);

  while(istream.IsOk() && ! istream.Eof()) {

    wxString line = input.ReadLine();

    if( line.StartsWith("VERBOSE") ) {
      verbose = GetBool(line);
      if( verbose )
	wxLog::SetLogLevel(wxLOG_Debug);
      else
	wxLog::SetLogLevel(wxLOG_Message);
      wxLog::SetVerbose();
    }

    if( line.StartsWith("OBJECT") )
      object = GetString(line);

  }

  if( object.IsEmpty() ) {
    fprintf(stderr,"STOP 'Sesame resolver got a blank object name.'\n");
    return 1;
  }

  VOSesame sezam(object);
  if( sezam.Resolve() ) {
    fprintf(stdout,"%.8f %.8f\n",sezam.GetRa(),sezam.GetDec());
    fprintf(stderr,"STOP 0\n");
    return 0;
  }
  else {
    fprintf(stderr,"STOP '%s'\n",sezam.GetErrMsg());
    return 1;
  }
}
