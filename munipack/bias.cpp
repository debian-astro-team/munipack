/*

  Average of biases

  Copyright © 2012-3, 2018 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"


bool Munipack::bias(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputSingle(action,cmd,"bias");
  CommonOptionsBitpix(action,cmd);
  CommonOptionsPhCorr(action,cmd);

  WriteFiles(action,cmd.GetFiles());

  return true;
}
