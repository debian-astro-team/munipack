/*

  VO Table manipulations

  Copyright © 2011-3,5,7-8 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/wx.h>
#include <wx/filename.h>

using namespace std;


bool Munipack::votable(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputSingle(action,cmd);

  wxString a, type;
  double x;
  long l;

  if( cmd.Found("o",&a) || cmd.Found("output",&a) ) {
    wxFileName fn(a);
    if( fn.IsOk() )
      type = fn.GetExt();
  }

  if( cmd.Found("type",&a) )
    type = a;

  if( ! type.IsEmpty() )
    action->Write("TYPE = '"+type.Upper()+"'");

  if( cmd.Found("pt",&a) )
    action->Write("PROJ TYPE = '" + a.MakeUpper() + "'");

  if( cmd.Found("pa",&x) )
    action->Write("PROJ ALPHA = %lf",x);

  if( cmd.Found("pd",&x) )
    action->Write("PROJ DELTA = %lf",x);

  if( cmd.Found("pw",&l) )
    action->Write("PROJ WIDTH = %ld",l);

  if( cmd.Found("ph",&l) )
    action->Write("PROJ HEIGHT = %ld",l);

  if( cmd.Found("ps",&x) )
    action->Write("PROJ SCALE = %lf",x);

  if( cmd.Found("ml",&x) )
    action->Write("MAG LIMIT = %lf",x);

  if( cmd.Found("col-mag",&a) )
    action->Write("COL_MAG = '" + a + "'");

  if( cmd.Found("col-ra",&a) )
    action->Write("COL_RA = '" + a + "'");

  if( cmd.Found("col-dec",&a) )
    action->Write("COL_DEC = '" + a + "'");

  WriteFiles(action,cmd.GetFiles());

  return true;
}
