/*

  Average of flats.

  Copyright © 2010-2014, 2017-8 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"


bool Munipack::flat(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputSingle(action,cmd);
  CommonOptionsBitpix(action,cmd);
  CommonOptionsPhCorr(action,cmd);
  EnvironmentPhCorr(action);

  double x;
  wxString a;

  if( cmd.Found("bias",&a) )
    action->Write("BIAS = '"+a+"'");

  if( cmd.Found("dark",&a) )
    action->Write("DARK = '"+a+"'");

  if( cmd.Found("xdark",&x) )
    action->Write("XDARK = %lf",x);

  if( cmd.Found("gain",&x) )
    action->Write("GAIN = %lf",x);

  if( cmd.Found("approximation",&a) )
    action->Write("APPROXIMATION = '" + a.Upper() + "'");

  WriteFiles(action,cmd.GetFiles());

  return true;
}
