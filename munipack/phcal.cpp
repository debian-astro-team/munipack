/*

  Photometric calibration

  Copyright © 2012-23 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include "vocatconf.h"
#include <wx/wx.h>
#include <wx/filename.h>


bool Munipack::phcal(MuniProcess *action, MuniCmdLineParser& cmd)
{
  wxString vocat;

  CommonOutputMultiple(action,cmd);

  wxString a;
  if( !  cmd.Found("O") &&
      ! (cmd.Found("target-directory",&a) || cmd.Found("t",&a)) ) {
    SetAdvanced(true);
    SetMask("\\1_cal.\\2");
  }

  if( cmd.Found("phsystab",&a) )
    action->Write("PHSYSTABLE = '" + a + "'");
  else if( FindDataDir("photosystems.fits",&a) )
    action->Write("PHSYSTABLE = '" + a + "'");

  if( cmd.Found("vocat",&a) )
    vocat = a;
  else if( FindDataDir("VOcat_conf.xml",&a) )
    vocat = a;

  if( cmd.Found("list") )

    action->Write("LIST = T");

  else {

    long n;
    double x;
    wxString a,c;

    if( cmd.Found("advanced") )
      action->Write("ADVANCED = T");

    if( cmd.Found("f",&a) || cmd.Found("filters",&a) ) {
      long i;
      apstr(a,i,c);
      action->Write("NFILTERS = %ld",i);
      action->Write("FILTERS = " + c);
    }

    if( cmd.Found("col-mag",&a) ) {
      long i;
      apstr(a,i,c);
      action->Write("COL_NMAG = %ld",i);
      action->Write("COL_MAG = " + c);
    }

    if( cmd.Found("col-magerr",&a) ) {
      long i;
      apstr(a,i,c);
      action->Write("COL_NMAGERR = %ld",i);
      action->Write("COL_MAGERR = " + c);
    }

    if( cmd.Found("q",&a) || cmd.Found("quantity",&a) ) {
      long i;
      apstr(a,i,c);
      action->Write("NQUANTITIES = %ld",i);
      action->Write("QUANTITIES = " + c);
    }

    if( cmd.Found("tratab",&a) )
      action->Write("TRATABLE = '" + a + "'");

    if( cmd.Found("photsys-instr",&a) )
      action->Write("PHOTSYS_INSTR = '"+a+"'");

    if( cmd.Found("photsys-ref",&a) )
      action->Write("PHOTSYS_REF = '"+a+"'");

    if( cmd.Found("tol",&x) )
      action->Write("TOL = %e",x);

    if( cmd.Found("area",&x) )
      action->Write("AREA = %e",x);

    if( cmd.Found("saper",&n) )
      action->Write("SAPER = %ld",n);

    if( cmd.Found("th",&x) || cmd.Found("threshold",&x))
      action->Write("THRESHOLD = %e",x);

    if( cmd.Found("e",&x) || cmd.Found("maxerr",&x))
      action->Write("MAXERR = %e",x);

    if( cmd.Found("apcorr",&x))
      action->Write("APCORR = %e",x);

    if( cmd.Found("C",&a) || cmd.Found("cal",&a) ) {
      long n;
      apstr(a,n,c);
      action->Write("NCTPH = %ld",n);
      action->Write("CTPH = " + a);
    }

    if( cmd.Found("r",&a) || cmd.Found("ref",&a) )
      action->Write("REF = '" + a + "'");

    if( cmd.Found("c",&c) || cmd.Found("cat",&c) ) {

      VOCatConf catalogs(vocat);
      VOCatResources cat(catalogs.GetCatFits(c));

      if( cmd.Found("col-ra",&a) )
	action->Write("COL_RA = '" + a + "'");
      else if( cat.IsOk() )
	action->Write("COL_RA = '" + cat.GetLabel("POS_EQ_RA") + "'");

      if( cmd.Found("col-dec",&a) )
	action->Write("COL_DEC = '" + a + "'");
      else if( cat.IsOk() )
	action->Write("COL_DEC = '" + cat.GetLabel("POS_EQ_DEC") + "'");

      action->Write("CAT = '" + c + "'");
    }

    WriteFiles(action,cmd);
  }

  return true;
}
